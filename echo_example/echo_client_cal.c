#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ctype.h>
#include <stdbool.h>

#define BUF_SIZE 1024

typedef struct calc_set_ {
    int digit;
	char priority;
    char oper;
	char next_index;
} calc_set;


void error_handling(char *message);
int message_to_calcset(char *message,calc_set *calc_list);

int main(int argc, char *argv[])
{
	bool cal_mode = false;

	calc_set calc_list[BUF_SIZE];
	int calc_list_size = 0;

	int sock;
	char message[BUF_SIZE] = {0};
	int str_len, recv_len, recv_cnt;
	struct sockaddr_in serv_adr;
	int port = 46460;
	
	if(argc==2) {
		printf("set default port = %d\n", port);
	} else if(argc!=3) {
		printf("Usage : %s <IP> <port>\n", argv[0]);
		exit(1);
	} else {
		port = atoi(argv[2]);
	}
	
	sock=socket(PF_INET, SOCK_STREAM, 0);   
	if(sock==-1)
		error_handling("socket() error");

	struct hostent *host;
	host = gethostbyname(argv[1]);

	printf("server ip: %s\n", inet_ntoa(*(struct in_addr*)host->h_addr_list[0]));

	memset(&serv_adr, 0, sizeof(serv_adr));
	serv_adr.sin_family=AF_INET;
	serv_adr.sin_addr.s_addr=inet_addr(inet_ntoa(*(struct in_addr*)host->h_addr_list[0]));
	serv_adr.sin_port=htons(port);
	
	if(connect(sock, (struct sockaddr*)&serv_adr, sizeof(serv_adr))==-1)
		error_handling("connect() error!");
	else
		puts("Connected!");
	
	while(1) 
	{
		memset(calc_list, 0x00, sizeof(calc_list));
		if(cal_mode)
		{
			fputs("Input formula(Q to quit): ", stdout);
			fgets(message, BUF_SIZE, stdin);
			calc_list_size = message_to_calcset(message,calc_list);
			write(sock, &calc_list_size, sizeof(int));
			int i=0;
			for(i=0; i<calc_list_size; i++){
				write(sock, &calc_list[i], sizeof(calc_set));
			}
			
			int cal_answer = 0;
			recv_cnt=read(sock, &cal_answer, sizeof(int));
			printf("answer = %d\n",cal_answer);
		}
		else
		{
			fputs("Input message(Q to quit): ", stdout);
			fgets(message, BUF_SIZE, stdin);
		
			if(!strcmp(message,"q\n") || !strcmp(message,"Q\n"))
				break;

			str_len=write(sock, message, strlen(message));
			
			recv_len=0;
			while(recv_len<str_len)
			{
				recv_cnt=read(sock, &message[recv_len], BUF_SIZE-1);
				if(recv_cnt==-1)
					error_handling("read() error!");
				recv_len+=recv_cnt;
			}
			
			message[recv_len]=0;
			printf("Message from server: %s", message);
			if(strncmp(message, "cal", 3)==0)
			{
				cal_mode = true;
			}
		}
	}
	
	close(sock);
	return 0;
}

int message_to_calcset(char *message,calc_set *calc_list){

	int message_size = (int)strlen(message);
	int i=0;
	int digit_size=0;
	int list_size=0;
	int priority=0;
	char *p_separator = message;
	
	for(i=0; i<message_size; i++){
				
		if(!isdigit(message[i]))
		{
			if(message[i] == '('){
				priority++;
			} else if (message[i] == ')'){
				priority--;
			}else{
				char atoi_digit[BUF_SIZE] = {0};
				strncpy(atoi_digit, p_separator, digit_size);
				calc_list[list_size].digit = atoi(atoi_digit);
				calc_list[list_size].priority = priority;
				calc_list[list_size].oper = message[i];
				digit_size=0;
				list_size++;
				p_separator = &message[i+1];
			}
		}
		else
		{
			digit_size++;
		}
	}
	return list_size;
}


void error_handling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}
