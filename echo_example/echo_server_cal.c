#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <ctype.h>
#include <stdbool.h>

#define BUF_SIZE 1024
void error_handling(char *message);

typedef struct calc_set_ {
    int digit;
	char priority;
    char oper;
	char next_index;
} calc_set;

int main(int argc, char *argv[])
{
	calc_set calc_list[BUF_SIZE];
	memset(&calc_list, 0, sizeof(calc_list));
	int calc_list_size = 0;
	int serv_sock, clnt_sock;
	char message[BUF_SIZE];
	int str_len, i;
	
	int service_port = 46460;

	struct sockaddr_in serv_adr;
	struct sockaddr_in clnt_adr;
	socklen_t clnt_adr_sz;
	bool cal_mode = false;

	if(argc!=2) {
		printf("set default port = %d\n", service_port);
	}else{
		service_port = atoi(argv[1]);
	}
	
	serv_sock=socket(PF_INET, SOCK_STREAM, 0);   
	if(serv_sock==-1)
		error_handling("socket() error");
	
	memset(&serv_adr, 0, sizeof(serv_adr));
	serv_adr.sin_family=AF_INET;
	serv_adr.sin_addr.s_addr=htonl(INADDR_ANY);
	serv_adr.sin_port=htons(service_port);
	
	int      option=1;
	setsockopt( serv_sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option) );
	
	if(bind(serv_sock, (struct sockaddr*)&serv_adr, sizeof(serv_adr))==-1)
		error_handling("bind() error");
	
	if(listen(serv_sock, 5)==-1)
		error_handling("listen() error");
	
	clnt_adr_sz=sizeof(clnt_adr);

	for(i=0; i<5; i++)
	{
		clnt_sock=accept(serv_sock, (struct sockaddr*)&clnt_adr, &clnt_adr_sz);
		if(clnt_sock==-1)
			error_handling("accept() error");
		else
			printf("Connected client %d \n", i+1);
		


		while((str_len=read(clnt_sock, message, BUF_SIZE))!=0) {
			
			if(strncmp(message, "cal", 3)==0){
				cal_mode = true;
				write(clnt_sock, "calculator mode\n", sizeof("calculator mode\n"));
				break;
			}else
				write(clnt_sock, message, str_len);
			
			memset(message,0x00,BUF_SIZE);
		}
		
		while(1){
			if(cal_mode)
			{
				int answer = 0;
				read(clnt_sock, &calc_list_size, sizeof(int));
				int i = 0;
				for(i=0; i<calc_list_size; i++){
					read(clnt_sock, &calc_list[i], sizeof(calc_set));
					calc_list[i].next_index = i+1;
				}
				calc_list[calc_list_size-1].next_index = -1;

				int cur_index = 0;
				while(true){

					int next_index = calc_list[cur_index].next_index;
					if(calc_list[cur_index].oper == '*'){ 
						calc_list[cur_index].digit *= calc_list[next_index].digit;
						calc_list[cur_index].oper = calc_list[next_index].oper; 
						calc_list[cur_index].next_index = calc_list[next_index].next_index;
						calc_list_size--;
					}
					else if(calc_list[cur_index].oper == '/'){
						calc_list[cur_index].digit /= calc_list[next_index].digit; 
						calc_list[cur_index].oper = calc_list[next_index].oper; 
						calc_list[cur_index].next_index = calc_list[next_index].next_index;
						calc_list_size--;
					}else{
						cur_index = calc_list[cur_index].next_index;
					}
					if(cur_index == -1)
						break;
				}
				
				answer = calc_list[0].digit;
				cur_index = 0;
				for(i=0; i<calc_list_size-1; i++){
					int next_index = calc_list[cur_index].next_index;
					if(calc_list[cur_index].oper == '+'){ answer+= calc_list[next_index].digit; }
					else if(calc_list[cur_index].oper == '-'){ answer-= calc_list[next_index].digit; }
					cur_index = calc_list[cur_index].next_index;
				}
				write(clnt_sock, &answer, sizeof(int));
				printf("oper answer = %d\n",answer);
				sleep(2);
			}
		}
		

		close(clnt_sock);
	}

	close(serv_sock);
	return 0;
}


void error_handling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}